import cv2
import copy
import math
import numpy as np

global image_hsv, pixel
image_hsv = None   # global ;(
pixel = (20,60,80) # some stupid default

def find_blue(frame, hsv_frame):
    # Blue color
    # hsv
    low_blue = np.array([190/2, 0.2 * 255, 0.2 * 255])
    high_blue = np.array([260/2, 255, 255])
    # low_blue = np.array([110,50,50])
    # high_blue = np.array([130,255,255])
    blue_mask = cv2.inRange(hsv_frame, low_blue, high_blue)
    blue = cv2.bitwise_and(frame, frame, mask=blue_mask)

    return blue, blue_mask


def test():
    print('hi!')

def pick_color(event,x,y,flags,param):
    print("pick!")
    if event == cv2.EVENT_LBUTTONDOWN:
        pixel = image_hsv[y,x]

        #you might want to adjust the ranges(+-10, etc):
        upper =  np.array([pixel[0] + 10, pixel[1] + 10, pixel[2] + 40])
        lower =  np.array([pixel[0] - 10, pixel[1] - 10, pixel[2] - 40])
        print(pixel, lower, upper)

        image_mask = cv2.inRange(image_hsv,lower,upper)
        cv2.imshow("mask",image_mask)
        
def find_largest_contour(countours):
    largest = countours[0]
    largest_area = cv2.contourArea(countours[0])
    for c in countours:
        area = cv2.contourArea(c)
        if area > largest_area:
            largest = c
            largest_area = area

    return largest


def hough(src):
    dst = cv2.Canny(src, 50, 200, None, 3)

    # Copy edges to the images that will display the results in BGR
    cdst = cv2.cvtColor(dst, cv2.COLOR_GRAY2BGR)
    cdstP = np.copy(cdst)

    lines = cv2.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)

    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
            cv2.line(cdst, pt1, pt2, (0,0,255), 3, cv2.LINE_AA)


    linesP = cv2.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)

    if linesP is not None:
        for i in range(0, len(linesP)):
            l = linesP[i][0]
            cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0,0,255), 3, cv2.LINE_AA)

    cv2.imshow("Source", src)
    cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)
    cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)

def detect_corners(img):
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray,2,3,0.04)
    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)
    # Threshold for an optimal value, it may vary depending on the image.
    img[dst>0.01*dst.max()]=[0,0,255]
    cv2.imshow('corners',img)

def draw_contour(cnt, img):
    cnt_area = cv2.contourArea(cnt)
    cnt_epsilon = 0.1 * cv2.arcLength(cnt, True)
    cnt_approx = cv2.approxPolyDP(cnt, cnt_epsilon, True)
    r = cv2.minAreaRect(cnt)
    b = cv2.boxPoints(r)
    b = np.int0(b)

    close_approx = cv2.approxPolyDP(cnt, 0.02*cv2.arcLength(cnt, True), True)

    cv2.drawContours(img, [close_approx], 0, (0, 255, 0), 5)
    i = cv2.drawContours(img, [b], 0, (0, 0, 255), 2)
    cnt_x = cnt_approx.ravel()[0]
    cnt_y = cnt_approx.ravel()[1]
    cv2.putText(i, "Board", (cnt_x, cnt_y), font, 1, (0, 0, 0))

def nothing(x):
    # any operation
    pass

# cap = cv2.VideoCapture(0)
# cap = cv2.VideoCapture('videos/klask.mov')
# cap = cv2.VideoCapture('videos/klask_2_7k.mp4')
cap = cv2.VideoCapture('videos/klaski_duct.mov')

cv2.namedWindow("Trackbars")
cv2.createTrackbar("L-H", "Trackbars", 0, 180, nothing)
cv2.createTrackbar("L-S", "Trackbars", 66, 255, nothing)
cv2.createTrackbar("L-V", "Trackbars", 134, 255, nothing)
cv2.createTrackbar("U-H", "Trackbars", 180, 180, nothing)
cv2.createTrackbar("U-S", "Trackbars", 255, 255, nothing)
cv2.createTrackbar("U-V", "Trackbars", 243, 255, nothing)

font = cv2.FONT_HERSHEY_COMPLEX

while cap.isOpened():
# while True:
    _, frame = cap.read()
    board_frame = copy.deepcopy(frame)

    # blur = cv2.blur(frame, (25, 25))
    # blur = cv2.bilateralFilter(frame, 9, 75, 75)
    # blur = cv2.medianBlur(frame,5)
    blur = cv2.GaussianBlur(frame,(5,5),0)
    hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
    hough(blur)

    detect_corners(blur)

    grey_frame = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
    threshed_frame = cv2.adaptiveThreshold(grey_frame,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    blue, blue_mask = find_blue(blur, hsv)

    l_h = cv2.getTrackbarPos("L-H", "Trackbars")
    l_s = cv2.getTrackbarPos("L-S", "Trackbars")
    l_v = cv2.getTrackbarPos("L-V", "Trackbars")
    u_h = cv2.getTrackbarPos("U-H", "Trackbars")
    u_s = cv2.getTrackbarPos("U-S", "Trackbars")
    u_v = cv2.getTrackbarPos("U-V", "Trackbars")

    lower_red = np.array([l_h, l_s, l_v])
    upper_red = np.array([u_h, u_s, u_v])

    mask = cv2.inRange(hsv, lower_red, upper_red)
    kernel = np.ones((5, 5), np.uint8)
    mask = cv2.erode(mask, kernel)

    # Contours detection
    # if int(cv2.__version__[0]) > 3:
        # Opencv 4.x.x
        # contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours, _ = cv2.findContours(blue_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    board = find_largest_contour(contours)
    draw_contour(board, board_frame)


    for cnt in contours:
        area = cv2.contourArea(cnt)
        epsilon = 0.1 * cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, epsilon, True)
        # approx = cv2.approxPolyDP(cnt, 0.02*cv2.arcLength(cnt, True), True)
        x = approx.ravel()[0]
        y = approx.ravel()[1]

        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        if area > 400:
            im = cv2.drawContours(frame, [box], 0, (0, 0, 255), 2)
            # cv2.drawContours(frame, [approx], 0, (0, 0, 0), 5)

            # if len(approx) == 3:
            #     cv2.putText(frame, "Triangle", (x, y), font, 1, (0, 0, 0))
            # elif len(approx) == 4:
            if len(approx) == 4:

                cv2.putText(frame, "Rectangle", (x, y), font, 1, (0, 0, 0))
            # elif 10 < len(approx) < 20:
            #     cv2.putText(frame, "Circle", (x, y), font, 1, (0, 0, 0))

    cv2.namedWindow('Frame')
    cv2.namedWindow('hsv')

    cv2.imshow("Frame", frame)
    cv2.setMouseCallback('Frame', pick_color)
    cv2.setMouseCallback('hsv', pick_color)

    # now click into the hsv img , and look at values:
    image_hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    cv2.imshow("hsv",image_hsv)


    # cv2.imshow("hsv", hsv)
    # cv2.imshow("thresh", threshed_frame)
    cv2.imshow("Blue", blue)
    cv2.imshow("board", board_frame)
    cv2.setMouseCallback('board', test)

    # key = cv2.waitKey(1)
    # if key == 27:
    #     break
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()