import numpy as np
import cv2
import imutils

from shape_detector import ShapeDetector

def find_blue(frame, hsv_frame):
    # Blue color
    low_blue = np.array([94, 80, 2])
    high_blue = np.array([126, 255, 255])
    blue_mask = cv2.inRange(hsv_frame, low_blue, high_blue)
    blue = cv2.bitwise_and(frame, frame, mask=blue_mask)

    return blue

def classify_contours(frame, hsv_frame):
    low_blue = np.array([94, 80, 2])
    high_blue = np.array([126, 255, 255])
    blue_mask = cv2.inRange(hsv_frame, low_blue, high_blue)
    kernel = np.ones((5, 5), np.uint8)
    mask = cv2.erode(blue_mask, kernel)
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        classify_contour(c)

    return frame, mask

def classify_contour(cnt):
    font = cv2.FONT_HERSHEY_SIMPLEX
    area = cv2.contourArea(cnt)
    approx = cv2.approxPolyDP(cnt, 0.02 * cv2.arcLength(cnt, True), True)
    x = approx.ravel()[0]
    y = approx.ravel()[1]
    if area > 400:
        cv2.drawContours(frame, [approx], 0, (0, 0, 0), 5)
        if len(approx) == 3:
            cv2.putText(frame, "Triangle", (x, y), font, 1, (0, 0, 0))
        elif len(approx) == 4:
            cv2.putText(frame, "Rectangle", (x, y), font, 1, (0, 0, 0))
        elif 10 < len(approx) < 20:
            cv2.putText(frame, "Circle", (x, y), font, 1, (0, 0, 0))

def bw_img(frame):
    blurred_frame = cv2.GaussianBlur(frame, (5, 5), 0)
    hsv = cv2.cvtColor(blurred_frame, cv2.COLOR_BGR2HSV)

    lower_blue = np.array([38, 86, 0])
    upper_blue = np.array([121, 255, 255])
    mask = cv2.inRange(hsv, lower_blue, upper_blue)

    contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    for contour in contours:
        cv2.drawContours(frame, contour, -1, (0, 255, 0), 3)

    return {
        'frame': frame,
        'mask': mask
    }
    # cv2.imshow("Frame", frame)
    # cv2.imshow("Mask", mask)

def read_img(image):
    resized = imutils.resize(image, width=300)
    ratio = image.shape[0] / float(resized.shape[0])

    gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    sd = ShapeDetector()

    for c in cnts:
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
        M = cv2.moments(c)
        cX = int((M["m10"] / M["m00"]) * ratio)
        cY = int((M["m01"] / M["m00"]) * ratio)
        shape = sd.detect(c)

        # multiply the contour (x, y)-coordinates by the resize ratio,
        # then draw the contours and the name of the shape on the image
        c = c.astype("float")
        c *= ratio
        c = c.astype("int")
        cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
        cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, (255, 255, 255), 2)

        return image
        # show the output image
        #cv2.imshow("Image", image)
        #cv2.waitKey(0)

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    blue_in_img = find_blue(frame, hsv_frame)
    f, m = classify_contours(frame, hsv_frame)
    cv2.imshow('Frame', f)
    cv2.imshow('Mask', m)
    # Our operations on the frame come here
    # img = cv2.cvtColor(frame, cv2.IMREAD_COLOR)
    # processed_img = read_img(img)
    processed_img = bw_img(frame)
    # Display the resulting frame
    # cv2.imshow('frame',processed_img)
    # cv2.imshow("Frame", processed_img.get('frame'))
    # cv2.imshow("Blue", blue_in_img)
    # cv2.imshow("Mask", processed_img.get('mask'))
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()